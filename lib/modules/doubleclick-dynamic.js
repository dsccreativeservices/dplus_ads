/**
 * Gets dynamic data
 *
 * @param {String} mode Indicates whether to return test data or full dynamic data.
 * Accepts two values, 'live' or 'dev'. dev will return local test data. 'live' will return true dynamic data,
 * but will only work if the banner is uploaded to Studio.
 * @param {string} template
 * @param {string} size
 */
import data300x250 from "../../src/brand_content/300x250/getData.js";
import devData160x600 from "../../src/brand_content/160x600/getData.js";

export function getDynamicData(mode, template, size) {
	return {
		'300x250': data300x250,
		'160x600': devData160x600
	}[size](mode, template);
}
