const bs = require('browser-sync').create();
const hidden = /(^|[/\\])\../; // .DS_Store etc

bs.init({
  watchOptions: {
    ignoreInitial: true,
    ignored: ['*.zip', hidden]
  },
  server: 'dist',
  files: 'dist',
  directory: true,
  injectChanges: false,
  reloadDebounce: 750,
  notify: false,
  logLevel: 'info',
  logFileChanges: false
});