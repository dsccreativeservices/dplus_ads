// Variables
var gridButton = document.getElementById('icon-grid'),
    listButton = document.getElementById('icon-list'),
    banners = document.querySelectorAll('.banners'),
    currentView,
    backupURL = '',
    preview = document.getElementById('preview'),
    previewImage = document.getElementById('preview-image'),
    listItems = document.querySelectorAll('.banners li'),
    i = listItems.length,
    // imgSize = '160x600',
    imgW = 160,
    imgH = 600;

// URL hash
function setHash(anchor) {
  var hash = (/^#/.test(anchor)) ? anchor : '#' + anchor;
  (history.pushState) ? history.pushState(null, null, hash) : location.hash = hash;
}

// Backup image folows mouse position
function followMouse(e) {
  var thumbWidth = imgW * 0.5;
  var thumbHeight = imgH * 0.5;
  var pad = 10;
  var scrollbar = 16;
  var translateX = (e.clientX + thumbWidth + pad + scrollbar < window.innerWidth) ? e.pageX + pad : e.pageX - pad - thumbWidth;
  var translateY = (e.clientY + thumbHeight + pad + scrollbar < window.innerHeight) ? e.pageY + pad : e.pageY - pad - thumbHeight;
  preview.style.transform = 'translate(' + translateX + 'px, ' + translateY + 'px';
}

// Switch navigation views
function switchView(view) {
  if((view === 'grid' || view === 'list') && currentView !== view) {
    gridButton.classList.toggle('icon-inactive');
    listButton.classList.toggle('icon-inactive');

    var bannersLength = banners.length;

    while(bannersLength--) {
      banners[bannersLength].classList.toggle('grid');
      banners[bannersLength].classList.toggle('list');
    }

    currentView = view;
    setHash(currentView);

    if(currentView === 'list') {
      document.addEventListener('mousemove', followMouse);
    } else {
      document.removeEventListener('mousemove', followMouse);
    }
  }
}

// Events
gridButton.addEventListener('click', function() {
  switchView('grid');
});

listButton.addEventListener('click', function() {
  switchView('list');  
});

// Show backup image when hovering on list view
while(i--){
  listItems[i].addEventListener('mouseover', function(e) {
    backupURL = e.target.dataset.backup; // dataset: ie11+
    imgW = e.target.dataset.width;
    imgH = e.target.dataset.height;

    if(backupURL !== 'undefined' && currentView === 'list') {
      preview.style.display = 'inline';
      previewImage.style.width = Math.round(imgW * 0.5) + 'px';
      previewImage.style.height = Math.round(imgH * 0.5) + 'px';
      previewImage.style.backgroundImage = 'url(' + backupURL + ')';
    }
  });

  listItems[i].addEventListener('mouseout', function() {
    backupURL = '';
    preview.style.display = 'none';
    previewImage.style.backgroundImage = 'url()';
  });
}

window.onload = function() {
  switchView(window.location.hash);
};