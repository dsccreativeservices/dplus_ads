const SVGSpriter = require('svg-sprite');
const fs = require('fs-extra');
const glob = require('glob');
const svgName = 'sprite.svg';
const cssName = 'sprite.svg.css';
const deleteFiles = require('./delete-files');

function createSpriteSVG(srcdir, outdir) {
  const spriter = new SVGSpriter({
    shape: {
      id: {
        generator: (name, file) => {
          // i shouldn't have to do this but name is getting lost https://github.com/jkphl/svg-sprite/issues/176
          return file.relative.replace(/\.svg$/, '');
        }
      },
      spacing: {
        padding: 1,
        box: 'content'
      }
    },
    mode: {
      css: {
        hasCommon: true,
        common: 'sprite-svg',
        commonName: 'sprite-svg',
        render: {
          css: {
            template: 'lib/tasks/modules/templates/sprite-svg.mustache',
            dest: `${outdir}/${cssName}`
          }
        },
        prefix: '.sprite-svg.%s',
        dimensions: true,
        bust: false,
        sprite: `${outdir}/${svgName}`,
        dest: ''
      }
    }
  });
  glob.glob('*.svg', {cwd: srcdir}, (err, files) => {
    if(files.length > 0){
      files.forEach(file => {
        // console.log(file);
        spriter.add(
          `${srcdir}/${file}`,
          file,
          fs.readFileSync(`${srcdir}/${file}`, {encoding: 'utf-8'})
        );
      });
      spriter.compile((error, result /*, data*/) => {
        for (var type in result.css) {
          fs.outputFile(result.css[type].path, result.css[type].contents);
        }
      });
    } else {
      console.log(`no svgs found in: ${srcdir}`);
      deleteSpriteSVG(outdir);
    }
  });
}

function deleteSpriteSVG(outdir) {
  const files = [`${outdir}/${cssName}`, `${outdir}/${svgName}`];
  deleteFiles(files);
}

function spriteSVG(srcdir) {
  const outdir = srcdir.split('/').slice(0, -1).join('/');
  fs.pathExists(srcdir)
    .then(exists => exists ? createSpriteSVG(srcdir, outdir) : deleteSpriteSVG(outdir));
}

module.exports = spriteSVG;