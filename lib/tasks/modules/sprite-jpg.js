const Sprite = require('./sprite');

function spriteJPG(dir) {
  Sprite({
    srcdir: dir,
    outdir: dir.split('/').slice(0, -1).join('/'),
    format: 'jpg',
    quality: 100,
    background: [0, 0, 0, 255],
    imageName: 'sprite',
    className: 'sprite-jpg'
  });
}

module.exports = spriteJPG;