const fs = require('fs-extra');
const HTMLHint = require('htmlhint').HTMLHint; // https://github.com/htmlhint/HTMLHint
const pretty = require('pretty'); // https://github.com/jonschlinkert/pretty
const rules = {
  "fullname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": true,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true,
  "title-require": true
};
const prettyOpts = {
  unformatted: [
    'code',
    'pre',
    'em',
    'strong',
    'span',
    'br',
    'script'
  ]
};

function writeHTML(data, outPath, included) {
  data = pretty(data, prettyOpts); // auto-format
  fs.outputFile(outPath, data, err => {
    if(err) {
      console.log(err);
    } else {
      console.log(`\x1b[35m=> ${outPath}\x1b[0m`);
      if(included) included.forEach(include => console.log(`\x1b[35m   + ${include}\x1b[0m`));
    }
  });
}

function headTags(data, path) {
  const parentDir = path.split('/').slice(-1).toString().toLowerCase();

  // Replace the static title with the folder name
  data = data.replace('<title>Banner</title>', `<title>${parentDir}</title>`);

  // Check if folder name is WIDTHxHEIGHT and populate ad.size
  const hxw = parentDir.match(/\d{2,4}x\d{2,4}/i);

  if(hxw) {
    const dimensions = hxw[0].split(/[xX]/);
    const width = dimensions[0];
    const height = dimensions[1];
    data = data.replace(/<meta(.*?)\sname\s*=\s*["']ad\.size["'](.*?)\s*>[\n\r]*/ig, `<meta name="ad.size" content="width=${width},height=${height}">`);
  }

  return data;
}

function processHTML(src) {
  const outPath = src.replace(/src\b/, 'dist');
  const outDir = outPath.split('/').slice(0, -1).join('/');
  const includePattern = /<!--\s*include\s+(.*?)\s*-->/ig;
  let matches = [];
  let match;
  let pending;

  fs.readFile(src, 'utf8', (err, data) => {
    if (err) throw err;
    // error check
    const report = HTMLHint.verify(data, rules);
    if (report.length > 0) {
      console.log(`\x1b[35mhtml errors found on: ${src}\x1b[0m`);
      report.forEach(hint => {
        console.log(`  ${hint.line}:${hint.col} ${hint.message}`);
      });
    } else {
      console.log(`\x1b[35m${src} =>\x1b[0m`);
    }
    // look for inline js include comments
    while ((match = includePattern.exec(data)) !== null) {
      matches.push({full: match[0], path: match[1]});
    }
    let i = pending = matches.length;
    let included = [];
    if (i > 0) {
      while (i--) {
        const full = matches[i].full;
        const path = matches[i].path;
        const ext = path.split('.').pop();
        const tags = {
          js: ['<script>\n', '\n    </script>'],
          css: ['<style>\n', '\n    </style>'],
          svg: ['', ''],
          html: ['', '']
        };
        fs.readFile(path, (e, contents) => {
          let inline;
          if (e) {
            console.log(`readFile error: ${e.path}`);
            inline = `<!-- FILE NOT FOUND: ${path} -->`;
          } else if(!tags[ext]) {
            inline = `<!-- UNSUPPORTED FILE TYPE: ${path} -->`;
          } else {
            const images = contents.toString().match(/[^'"(\s]+?\.(jpg|jpeg|png|gif|svg)(?!\.)\b/g);
            const unique = [...new Set(images)];
            if(unique){
              const includeDir = path.split('/').slice(0, -1).join('/'); // relative to include file
              unique.forEach(image => {
                fs.copy(`${includeDir}/${image}`, `${outDir}/${image}`, { overwrite: true }, err => {
                  (err)
                    ? console.log(`\x1b[31mfile not copied: ${err.path}\x1b[0m`)
                    : console.log(`\x1b[35m=> ${outDir}/${image}\x1b[0m`);
                });
              });
            }
            inline = `${tags[ext][0]}${contents}${tags[ext][1]}`;
            included.push(path);
          }
          data = data.replace(full, inline);
          pending--;
          if (pending < 1) {
            data = headTags(data, outDir);
            writeHTML(data, outPath, included);
          }
        });
      }
    } else {
      data = headTags(data, outDir);
      writeHTML(data, outPath);
    }
  });
}

module.exports = processHTML;