const rollup = require('rollup');
const { babel } = require('@rollup/plugin-babel');
const { eslint } = require('rollup-plugin-eslint');
const { uglify } = require('rollup-plugin-uglify');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const commonjs = require('@rollup/plugin-commonjs');

const pluginsDev = [
  nodeResolve({
    mainFields: ['module', 'main', 'jsnext:main'],
    browser: true,
    customResolveOptions: {
      moduleDirectory: './lib/modules'
    }
  }),
  commonjs(),
  eslint({
    exclude: [
      'src/**/*.{css,scss,sass,less}'
    ]
  }),
  babel({
    exclude: 'node_modules/**',
    babelHelpers: 'bundled'
  })//, (/prod/i.test(env) && uglify())
];
const pluginsProd = [...pluginsDev, uglify()];

async function processJS(src, env){
  const plugins = (/prod/i.test(env)) ? pluginsProd : pluginsDev;
  try {
    console.log(`\x1b[33m${src} =>\x1b[0m`);
    const bundle = await rollup.rollup({
      input: src,
      plugins: plugins
    });
    const outpath = src.replace('src/', 'dist/').replace('/main.js', '/bundle.js');
    await bundle.write({
      file: outpath,
      format: 'iife'
    });
    console.log(`\x1b[33m=> ${outpath}\x1b[0m`);
  } catch (err) {
    console.log(`rollup: ${err}`);
  }
}

module.exports = processJS;