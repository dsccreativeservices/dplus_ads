const Sprite = require('./sprite');

function spritePNG(dir) {
  Sprite({
    srcdir: dir,
    outdir: dir.split('/').slice(0, -1).join('/'),
    format: 'png',
    imageName: 'sprite',
    className: 'sprite-png'
    // background: [255, 255, 255, 255],
  });
}

module.exports = spritePNG;