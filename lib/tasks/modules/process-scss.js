const sass = require('node-sass');
const fs = require('fs-extra');

function processSCSS(src) {
  const pathArr = src.split('/');
  const srcdir = pathArr.slice(0, -1).join('/');
  const distdir = srcdir.replace(/src\b/, 'dist');

  sass.render({
    file: src,
    includePaths: ['lib/sass/'],
    outputStyle: 'compact',
    sourceMapEmbed: true,
  }, (err, result) => {
    // console.log(result);
    if (err) {
      console.log(err);
    } else {
      const outfile = `${srcdir}/styles/main.css`;
      // const outMap = `${distdir}/bundle.css.map`;
      copyFonts(result.css.toString(), distdir);
      // console.log(result)
      fs.outputFile(outfile, result.css, err => {
        (err) ? console.log(err) : console.log(`\x1b[36m${src} =>\x1b[0m`);
      });
      // fs.outputFile(outMap, result.map, err => {
      //   (err) ? console.log(err) : console.log(`\x1b[36m${src} =>\x1b[0m`);
      // });
    }
  });
}

function copyFonts(cssText, dir) {
  const fonts = cssText.match(/[^'"(\s]+?\.(woff|woff2|ttf|eot)(?!\.)\b/g);
  if(fonts){
    fonts.forEach(font => {
      fs.copy(`lib/fonts/${font}`, `${dir}/${font}`, { overwrite: false }, err => {
        if (err) console.log('\x1b[31m%s\x1b[0m', `file not copied: ${err.path}`);
      });
    });
  }
}

module.exports = processSCSS;
