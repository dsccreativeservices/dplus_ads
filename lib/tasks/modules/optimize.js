const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imageminSvgo = require('imagemin-svgo');
const fs = require('fs-extra');
const { promisify } = require('util');
const deleteFiles = require('./delete-files');

const fsStatAsync = promisify(fs.stat);

async function optimizeImage(src, outfile) {
  const filename = src.split('/').slice(-1);
  const low = /[-_@][1-9]x./.test(filename); // more compression on @2x @3x... res (retina) images
  const mid = /^sprite|^backup/i.test(filename);
  const pngQual = low ? [0.3, 0.5] : [0.6, 0.8];
  const jpgQual = mid ? 79 : low ? 69 : 89;
  const files = await imagemin([src], {
    destination: outfile.split('/').slice(0, -1).join('/'),
    plugins: [
      imageminMozjpeg({quality: jpgQual}),
      imageminPngquant({strip: true, quality: pngQual}),
      imageminSvgo(),
    ]
  });
  files.forEach(file => {
    (async () => {
      const before = await fsStatAsync(file.sourcePath);
      const after = await fsStatAsync(file.destinationPath);
      console.log(`\x1b[94m=> ${file.destinationPath} (${(before.size * 0.001).toFixed(1)}k > ${(after.size * 0.001).toFixed(1)}k)\x1b[0m`);
      // console.log(`\x1b[94m=> ${file.destinationPath} (${(before.size * 0.001).toFixed(1)}k > ${(after.size * 0.001).toFixed(1)}k / -${((1 - (after.size / before.size)) * 100).toFixed(1)}%)\x1b[0m`);
    })();
  });
}

function optimize(src){
  const outfile = src.replace(/\bsrc\b/i, 'dist').replace(/\/styles\b/, '');
  fs.pathExists(src)
    .then(exists => exists ? optimizeImage(src, outfile) : deleteFiles([outfile]));
}

module.exports = optimize;