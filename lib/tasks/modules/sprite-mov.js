const fs = require('fs-extra');
const path = require('path');
const Spritesmith = require('spritesmith');
const imageSize = require('image-size');
const deleteFiles = require('./delete-files');

function movSprite(data){
  const total = data.sprites.length - 1;
  const last = data.sprites[total];
  if(total > 0) {
    return `.${data.spritesheet.name} {
  background-image: url('${data.spritesheet.image}');
  width: ${last.width}px;
  height: ${last.height}px; }
.${data.spritesheet.name}.trans-12fps {
  transition: background-position ${parseFloat((total * 0.083333).toFixed(4))}s steps(${total}); }
.${data.spritesheet.name}.trans-15fps {
  transition: background-position ${parseFloat((total * 0.066667).toFixed(4))}s steps(${total}); }
.${data.spritesheet.name}.trans-18fps {
  transition: background-position ${parseFloat((total * 0.055556).toFixed(4))}s steps(${total}); }
.${data.spritesheet.name}.trans-24fps {
  transition: background-position ${parseFloat((total * 0.041667).toFixed(4))}s steps(${total}); }
.${data.spritesheet.name}.trans-30fps {
  transition: background-position ${parseFloat((total * 0.033333).toFixed(4))}s steps(${total}); }
.${data.spritesheet.name}.trans-60fps {
  transition: background-position ${parseFloat((total * 0.016667).toFixed(4))}s steps(${total}); }
.${data.spritesheet.name}.last-frame {
  background-position: ${last.x * -1}px ${last.y * -1}px; }
.${data.spritesheet.name}.first-frame {
  background-position: 0 0; }
`;
  } else {
    return '';
  }
}

function createSpriteMOV(srcdir, outdir, className, imageName){
  let format = 'png';
  let algorithm = 'left-right';
  fs.readdir(srcdir, (err, files) => {
    if (err) {
      console.log(err);
    } else {
      const images = [];
      let gif = 0;
      let jpg = 0;
      let png = 0;
      let w = 0;
      let h = 0;
      files.forEach((file) => {
        if(/(\.(jpg|png|gif))$/i.test(file)){
          const ext = file.split('.').pop();
          const dimensions = imageSize(`${srcdir}/${file}`);
          if(w !== 0 && (w !== dimensions.width || h !== dimensions.height)){
            console.log('warning: dimensions of all images in sequence should be uniform');
          }
          w = dimensions.width;
          h = dimensions.height;
          if(ext === 'gif') gif++;
          if(ext === 'jpg') jpg++;
          if(ext === 'png') png++;
          images.push(`${srcdir}/${file}`);
        }
      });
      if(images.length > 0){
        const aspect = w / h; // uses the last image's dimensions
        if(gif && !jpg && !png) format = 'gif';
        else if(!gif && jpg && !png) format = 'jpg';
        else format = 'png';
        if(aspect > 1 && format === 'jpg') algorithm = 'top-down';
        if(aspect > 1.8 && format !== 'jpg') algorithm = 'top-down'; // gif/png compression works better horizontally
        // console.log(`${w}x${h}: ${algorithm}`);
        Spritesmith.run({
          src: images,
          algorithm: algorithm,
          algorithmOpts: {sort: false},
          padding: 0,
          exportOpts: {
            // using default Pixelsmith engine
            background: [0, 0, 0, 0], // [r,g,b,a] array of 0-255 integers (alpha ignored for jpeg)
            format: format,
            quality: 100
          }
        },
        function handleResult (err, result) {
          if (err) {
            throw err;
          }
          fs.outputFile(`${outdir}/${imageName}.${format}`, result.image);
          let sprites = Object.keys(result.coordinates).map(key => {
            const name = path.parse(key).name;
            result.coordinates[key].name = name;
            return result.coordinates[key];
          });
          const data = {
            sprites: sprites,
            spritesheet: {
              width: result.properties.width,
              height: result.properties.height,
              image: `${imageName}.${format}`,
              name: `${className}`
            }
          };
          let css = movSprite(data);
          fs.outputFile(`${outdir}/${imageName}.css`, css);
          // console.log(result);
        });
      } else {
        console.log(`no valid images found in: ${srcdir}`);
        deleteSpriteMOV(outdir, imageName);
      }
    }
  });
}

function deleteSpriteMOV(outdir, imageName){
  const files = [`${outdir}/${imageName}.css`, `${outdir}/${imageName}.gif`, `${outdir}/${imageName}.jpg`, `${outdir}/${imageName}.png`];
  deleteFiles(files);
}

function spriteMOV(srcdir){
  const outdir = srcdir.split('/').slice(0, -1).join('/');
  const className = srcdir.split('/').pop(); // 'sprite-mov';
  const imageName = className.replace('-', '.'); // 'sprite.mov';
  fs.pathExists(srcdir)
    .then(exists => exists ? createSpriteMOV(srcdir, outdir, className, imageName) : deleteSpriteMOV(outdir, imageName));
}

module.exports = spriteMOV;