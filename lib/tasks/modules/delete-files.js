const fs = require('fs-extra');

function deleteFile(file){
  fs.remove(file)
    .then(() => console.log(`\x1b[31mdeleted: ${file}\x1b[0m`))
    .catch(err => console.error(err));
}

function deleteFiles(files){
  files.forEach(file => {
    fs.pathExists(file)
      .then(exists => {
        if(exists) deleteFile(file);
      });
  });
}

module.exports = deleteFiles;