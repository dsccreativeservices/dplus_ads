const fs = require('fs-extra');
const path = require('path');
const targDir = process.argv[2] || 'dist';
const projectDir = path.resolve(targDir).split('/').slice(-2, -1)[0]; // parent dir of targDir
const glob = require('glob');
const sizeOf = require('image-size');
const file = fs.createWriteStream(`${targDir}/index.html`);
const css = fs.readFileSync(`${__dirname}/review-index-assets/main.css`).toString();
const js = fs.readFileSync(`${__dirname}/review-index-assets/main.js`).toString();
const rgaLogo = fs.readFileSync(`${__dirname}/review-index-assets/rga-logo.svg`).toString();

function groupBy(arr, property) {
  return arr.reduce((memo, x) => {
    if(!memo[x[property]]) memo[x[property]] = [];
    memo[x[property]].push(x);
    return memo;
  }, {});
}

glob(`${targDir}/*/**/index.html`, (err, files) => {
  const links = files.map(file => {
    const link = file.replace(`${targDir}/`, '');
    const group = link.split('/').slice(0,-2).join('/');
    const ad = link.split('/').slice(-2,-1)[0];
    let backup;
    ['jpg', 'png', 'gif'].forEach(ext => {
      const imgPath = (group) ? `${group}/${ad}/backup.${ext}` : `${ad}/backup.${ext}`;
      if(fs.existsSync(`dist/${imgPath}`)) backup = imgPath;
    });

    const dimensions = (backup) ? sizeOf(`dist/${backup}`) : {width: 300, height: 250};
    const width = dimensions.width;
    const height = dimensions.height;
    const size = `${dimensions.width}x${dimensions.height}`;
    return {group: group, link: link, ad: ad, size: size, backup: backup, width: width, height: height};
  });

  const groups = groupBy(links, 'group');
  
  const lists = Object.keys(groups).map((group) => {
    return `<article class="creatives clearfix">
        ${(group) ? `<h2 class="creative-title">${group}</h2>` : '' }

        <ul class="banners grid">
          ${groups[group].map(banner => {
            const imgW = Math.round(banner.width * 0.2);
            const imgH = Math.round(banner.height * 0.2);
            return `<li>
            <a href="${banner.link}" data-backup="${banner.backup}" data-size="${banner.size}" data-width="${banner.width}" data-height="${banner.height}">
              <div class="backup">
                ${(banner.backup) ? `<img width="${imgW}" height="${imgH}" src="${banner.backup}" alt="${banner.ad}">` : rgaLogo}
              </div>
              ${banner.ad}
            </a>
          </li>`;
          }).join('\n        ')}
        </ul>
      </article>`;
  });

  const html = `<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <title>${projectDir}</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
${css}
    </style>
  </head>

  <body>
    <header id="header" class="wrap clearfix">
      <div id="site-logo">
        <div id="rga-logo">
          ${rgaLogo}
        </div>

        <div id="site-name">Showcase</div>
      </div>
    </header>

    <section id="main-content" class="wrap clearfix">
      <div id="content-bar" class="clearfix">
        <h1 id="campaign-title">${projectDir}</h1>

        <nav id="toggle-view">
          <ul>
            <li>
              <svg id="icon-grid" width="25" height="25">
                <path d="M0 0h10v10H0V0zm15 0h10v10H15V0zM0 15h10v10H0V15zm15 0h10v10H15V15z" fill-rule="evenodd"/>
              </svg>
            </li>

            <li>
              <svg id="icon-list" class="icon-inactive" width="25" height="25">
                <path d="M0 0h25v5H0V0zm0 10h25v5H0v-5zm0 10h25v5H0v-5z" fill-rule="evenodd"/>
              </svg>
            </li>
          </ul>
        </nav>
      </div>

      ${lists.join('\n      ')}
    </section>
    
    <div id="preview">
      <div id="preview-image"></div>
    </div>
  </body>

  <script>
${js}
  </script>
</html>`;

  file.write(html);
  file.end();
});