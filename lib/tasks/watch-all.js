const chokidar = require('chokidar');
const debounce = require('lodash.debounce');
const fs = require('fs-extra');
const processHTML = require('./modules/process-html');
const processSCSS = require('./modules/process-scss');
const processJS = require('./modules/process-js');
const processCSS = require('./modules/process-css');
const spriteJPG = require('./modules/sprite-jpg');
const spriteMOV = require('./modules/sprite-mov');
const spritePNG = require('./modules/sprite-png');
const spriteSVG = require('./modules/sprite-svg');
const optimize = require('./modules/optimize');
const deleteFiles = require('./modules/delete-files');

const env = (process.argv[2]) ? process.argv[2] : 'dev';
const watcher = chokidar.watch('src', {
  ignored: /(^|[/\\])\../, // .DS_Store etc,
  persistent: true,
  ignoreInitial: true,
  usePolling: true,
  awaitWriteFinish: true
});
const directories = {};
const deb = debounce(executeTasks, 200);

function executeTasks() {
  for (const dir in directories) {
    let dirname = dir.split('/').pop();
    console.log(`${directories[dir].length} file events in ${dir}`);
    if (/sprite-/.test(dirname)) {
      if (/sprite-jpg/.test(dirname)) spriteJPG(dir);
      else if (/sprite-mov/.test(dirname)) spriteMOV(dir);
      else if (/sprite-png/.test(dirname)) spritePNG(dir);
      else if (/sprite-svg/.test(dirname)) spriteSVG(dir);
      else console.log(`unhandled sprite change: ${dir}`);
    } else {
      fs.pathExists(dir).then(exists => {
        (exists)
        ? console.log(`unhandled debounced change in: ${dir}`)
        : deleteFiles([dir.replace('src/', 'dist/')]);
      });
    }
  }
  for (let dir in directories) delete directories[dir];
}

function debounceEvents(dirpath, changed){
  if ({}.hasOwnProperty.call(directories, dirpath)) {
    directories[dirpath].push(changed);
  } else {
    directories[dirpath] = [changed];
  }
  deb();
}

function handleFileEvent(obj) {
  const patharr = obj.path.split('/');
  const filename = patharr.splice(-1, 1)[0];
  const dirpath = patharr.join('/');
  const changed = {
    name: filename,
    event: obj.event
  };
  fs.pathExists(dirpath).then(exists => {
    if (exists) {
      if (/sprite-/.test(dirpath)) {
        debounceEvents(dirpath, changed); // debounce sprite folders
      } else {
        // non-sprite changes
        if (/\.html$/i.test(filename)) processHTML(`${dirpath}/${filename}`);
        else if (/\.scss$/i.test(filename)) processSCSS(`${dirpath}/${filename}`);
        else if (/\.css$/i.test(filename)) processCSS(`${dirpath}/${filename}`);
        else if (/\.js$/i.test(filename)) {
          if (/\bmain.js$/i.test(filename)) processJS(`${dirpath}/${filename}`, env);
          else console.log(`non-entry js file change: ${dirpath}/${filename}`);
        }
        else if (/\.(gif|jpg|jpeg|png|svg)$/i.test(filename)) optimize(`${dirpath}/${filename}`);
        else console.log(`unhandled file change: ${dirpath}/${filename}`);
      }
    } else {
      debounceEvents(dirpath, changed); // files deleted
    }
  });
}

watcher.on('error', error => {
    console.log(`watch-all error: ${error}`);
  })
  .on('ready', () => {
    console.log('---------- watch-all ready ----------');
  })
  .on('add', path => {
    handleFileEvent({
      path: path,
      event: 'add'
    });
  })
  .on('unlink', path => {
    handleFileEvent({
      path: path,
      event: 'unlink'
    });
  })
  .on('change', path => {
    handleFileEvent({
      path: path,
      event: 'change'
    });
  });
