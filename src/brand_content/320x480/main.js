import {domIds, es5} from 'banner-utils';
import populateFrames from "../populateFrames.js";
import {getData} from "./getData.js";

const Banner = {
	init: () => {
		const dom = domIds();
		const adWidth = 320;
		const adHeight = 480;
		const mode = 'live';
		const arrow_wrap_initial = {x: 105, y: 20, scale: 1};
		const arrow1_corner = {x: -155, y: -236, scale: 0.8};
		const arrow2_corner = {x: 155, y: 235, scale: 0.8};

		const display = () => {
			const openFrame = (wrapper, image) =>
				gsap.timeline({defaults: {duration: 0.5, ease: 'power3.inOut'}})
					.set(wrapper, {autoAlpha: 1})
					.add("anim")
					.to("#arrow1", arrow1_corner, "anim")
					.to("#arrow_wrap", {scale: 1}, "anim")
					.to("#arrow2", arrow2_corner, "anim")
					.from(wrapper, {width: 0, height: 0, x: adWidth / 2, y: adHeight / 2, force3D: false}, "anim")
					.from(image, {x: -adWidth / 2, y: -adHeight / 2, force3D: false}, "anim");

			const centerArrows = () =>
				gsap.timeline({defaults: {duration: 0.5, ease: 'power3.inOut'}})
					.add("anim")
					.to("#arrow1", {x: 0, y: 0, scale: 1}, "anim")
					.to("#arrow2", {x: 0, y: 0, scale: 1}, "anim");

			const closeFrame = (wrapper, image) =>
				gsap.timeline({defaults: {duration: 0.5, ease: 'power3.inOut'}})
					.set(wrapper, {autoAlpha: 1})
					.add(centerArrows(), "anim")
					.to(wrapper, {width: 0, height: 0, x: adWidth / 2, y: adHeight / 2, force3D: false}, "anim")
					.to(image, {x: -adWidth / 2, y: -adHeight / 2, force3D: false}, "anim");

			const start_frame = () => {
				dom.ad_content.classList.remove('invisible');

				return gsap.timeline({defaults: {ease: 'expo.inOut'}})
					.to("#arrow_wrap", 0, arrow_wrap_initial)
					.to(["#logo", ".cta_f1"], 0.5, {scale: 0.95, autoAlpha: 0}, "+=1")
					.to("#arrow_wrap", 0.75, {autoAlpha:1}, "-=0.75")
					.to("#arrow_wrap", 0.75, {x: 0, y: 0, scale: 1.3}, "-=0.5")
					.to(".arrow", 0.75, {fill: "#FFFFFF"}, "-=0.75");
			};

			const rtb1_frame = () =>
				gsap.timeline({defaults: {ease: 'expo.inOut'}})
					.add(openFrame("#middle_frames", "#middle_frames_inner"))
					.add(centerArrows(), "+=3.5")
			;

			const rtb2_frame = () =>
				gsap.timeline({defaults: {ease: 'expo.inOut'}})
					.set("#key_message_cta", {autoAlpha: 1})
					.add(openFrame("#rtb2_mask", "#rtb2_wrap"))
					.add(closeFrame("#rtb_frames", "#rtb_frames_inner"), "+=3.5")
					.set(".logo_small_grey", {autoAlpha: 1}, "-=0.35")
			;

			const key_message_frame = () =>
				gsap.timeline({defaults: {ease: 'expo.inOut'}})
					.add(openFrame("#key_message_mask", ".key_message_wrap"))
					.to(".arrow", 0.75, {fill: "#4C4F55"}, "-=0.5")
					.add(closeFrame("#middle_frames", "#middle_frames_inner"), "+=3")
					.set(".logo_small_grey,#key_message_cta", {autoAlpha: 0}, "-=0.25");

			const end_frame = () => {
				const endFrameArrows = () =>
					gsap.timeline({defaults: {duration: 0.25, ease: 'power3.inOut'}})
						.to("#arrow_wrap", 0.5, arrow_wrap_initial, "-=0.5")
						.to(["#arrow1", "#arrow2"], {scale: 1}, "-=0.5");
				return gsap.timeline({defaults: {ease: 'expo.inOut'}})
					.add(endFrameArrows())
					.to(["#logo", ".cta_f1"], 0.5, {scale: 1, autoAlpha: 1}, "-=0.5")
					.to("#arrow_wrap", 0.25, {autoAlpha:0}, "-=0.25")
					;
			};

			const master_animation = () =>
				gsap.timeline()
					.add(start_frame())
					.add(rtb1_frame())
					.add(rtb2_frame())
					.add(key_message_frame())
					// .addPause()
					.add(end_frame());

			const parseData = () => {
				const data = getData(mode);

				populateFrames(adWidth, data, master_animation, img => {
					img.width = adWidth;
				});
			};

			if ( Enabler.isPageLoaded() ) {
				parseData();
			} else {
				Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, parseData);
			}
		};

		// Display the banner
		es5() ? display() : dom.backup.classList.add('backup');
	}
};

window.onload = () => {
	if ( Enabler.isInitialized() ) Banner.init();
	else Enabler.addEventListener(studio.events.StudioEvent.INIT, Banner.init);
};
