import {domIds, es5} from 'banner-utils';
import {getData} from "./getData.js";
import populateFrames from "../populateFrames.js";

const Banner = {
  init: () => {

    const dom = domIds();

    let adWidth = 300,
        adHeight = 250,
        exit_url = "";

    // Display the banner
    es5() ? display() : dom.backup.classList.add('backup');

    function display() {
      // Setup -------------------------------------------------
      /**
       * Parses incoming dynamic data
       */
      function parseData() {


        const data = getData('dev');

        populateFrames(adWidth, data, master_animation, img => {
          img.height = adHeight;
        })
      }

      /////// END PARSE //////

      /**
       * Main banner animations
       */

      function endFrameArrows() {

        const tl = gsap.timeline({defaults: {duration: 0.25, ease: 'power3.inOut'}});

        tl
            .to("#arrow_wrap", 0.5, {x: 0, y: 0, scale: 0.95}, "-=0.5");

        return tl;
      }

      function centerArrows() {

        const tl = gsap.timeline({defaults: {duration: 0.25, ease: 'power3.inOut'}});

        tl

            .add("anim")
            .to("#arrow1", {x: 0, y: 0}, "anim")
            .to("#arrow2", {x: 0, y: 0}, "anim");

        return tl;
      }

      function openFrame(wrapper, image) {

        const tl = gsap.timeline({defaults: {duration: 0.5, ease: 'power3.inOut'}});

        tl
            .set(wrapper, {autoAlpha: 1})
            .add("anim")
            .to("#arrow1", {x: -144, y: -117}, "anim")
            .to("#arrow2", {x: 141, y: 118}, "anim")
            .from(wrapper, {width: 0, height: 0, x: adWidth / 2, y: adHeight / 2, force3D: false}, "anim")
            .from(image, {x: -adWidth / 2, y: -adHeight / 2, force3D: false}, "anim");

        return tl;
      }

      function closeFrame(wrapper, image) {

        const tl = gsap.timeline({defaults: {duration: 0.5, ease: 'power3.inOut'}});

        tl
            .set(wrapper, {autoAlpha: 1})
            .add("anim")
            .to("#arrow1", {x: 0, y: 0}, "anim")
            .to("#arrow2", {x: 0, y: 0}, "anim")
            .to(wrapper, {width: 0, height: 0, x: adWidth / 2, y: adHeight / 2, force3D: false}, "anim")
            .to(image, {x: -adWidth / 2, y: -adHeight / 2, force3D: false}, "anim");

        return tl;
      }

      function f1_animation() {
        dom.ad_content.classList.remove('invisible');
        const tl = gsap.timeline({defaults: {ease: 'expo.inOut'}});

        tl
            .to(["#logo", ".cta_f1"], 0.5, {scale: 0.95, autoAlpha: 0}, "+=1")
            .to("#arrow_wrap", 0.75, {x: -75, y: -8, scale: 1}, "-=0.5")
            .to(".arrow", 0.75, {fill: "#FFFFFF"}, "-=0.75");


        return tl;

      }

      function f2_animation() {

        const tl = gsap.timeline({defaults: {ease: 'expo.inOut'}});

        tl
            .add(openFrame("#rtb1_mask", "#rtb1_wrap"))
            .add(centerArrows(), "+=3.5");

        return tl;

      }

      function f3_animation() {
        const tl = gsap.timeline({defaults: {ease: 'expo.inOut'}});

        tl
            .set("#key_message_cta", {autoAlpha: 1})
            .add(openFrame("#rtb2_mask", "#rtb2_wrap"))
            .add(closeFrame("#rtb2_mask", "#rtb2_wrap"), "+=3.5")
            .set(".logo_small_grey", {autoAlpha: 1}, "-=0.35")
            .set("#rtb1_mask", {autoAlpha: 0}, "-=1");

        return tl;

      }

      function f4_animation() {
        const tl = gsap.timeline({defaults: {ease: 'expo.inOut'}});

        tl
            .add(openFrame("#key_message_mask", ".key_message_wrap"))
            // .addPause()
            .to(".arrow", 0.75, {fill: "#4C4F55"}, "-=0.5")
            .add(closeFrame("#key_message_mask", ".key_message_wrap"), "+=3")
            .set(".logo_small_grey,#key_message_cta", {autoAlpha: 0}, "-=0.25");


        return tl;

      }

      function ef_animation() {
        const tl = gsap.timeline({defaults: {ease: 'expo.inOut'}});

        tl
            .add(endFrameArrows())
            .to(["#logo", ".cta_f1"], 0.5, {scale: 1, autoAlpha: 1}, "-=0.5");

        // .from(["#ef_logo,#cta,#ef_logos"],0.25,{autoAlpha:0,stagger:0.1})

        return tl;

      }

      function master_animation() {

        const tl = gsap.timeline({defaults: {ease: 'expo.inOut'}});

        tl
            .add(f1_animation())
            .add(f2_animation())
            // .addPause()

            .add(f3_animation())
            .add(f4_animation())
            // .addPause()
            .add(ef_animation());

        return tl;
      }

      /**
       * Check if page is loaded
       */

      if ( Enabler.isPageLoaded() ) parseData();
      else Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, parseData);


      // Events ------------------------------------------------

      function rollover() {
        dom.ad_content.addEventListener('mouseenter', () => {
          gsap.set(".cta", {background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 70%)"});
        });

        dom.ad_content.addEventListener('mouseleave', () => {
          gsap.set(".cta", {background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 100%)"});
        });
      }

      function clickThrough() {
        dom.ad_content.addEventListener('click', () => Enabler.exitOverride("main exit", exit_url));
      }
    }
  }
};

window.onload = () => {
  if ( Enabler.isInitialized() ) Banner.init();
  else Enabler.addEventListener(studio.events.StudioEvent.INIT, Banner.init);
};
