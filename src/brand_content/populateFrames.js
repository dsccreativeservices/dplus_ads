import {fitText} from "../../lib/modules/dynamic-utils.js";
import {domIds} from "../../lib/modules/banner-utils.js";

// Events ------------------------------------------------
let exit_url = "";
const dom = domIds();
function rollover() {
	dom.ad_content.addEventListener('mouseenter', () => {
		gsap.set(".cta", {background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 70%)"});
	});

	dom.ad_content.addEventListener('mouseleave', () => {
		gsap.set(".cta", {background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 100%)"});
	});
}

function clickThrough() {
	// dom.ad_content.addEventListener('click', () => Enabler.exitOverride("main exit", exit_url));
	dom.ad_content.addEventListener('click', () => Enabler.exit("main exit"));
}

export default (adWidth, data, master_animation, transforms=null) => {

	// Create and append an image
	function loadImg(imgUrl, domElement) {
		let fileType = imgUrl.substring(imgUrl.length - 3);
		const img = document.createElement('img');
		img.src = imgUrl;

		if ( fileType === "jpg" ) {
			if ( transforms ) {
				transforms(img);
			} else {
				img.width = adWidth;
			}
			// img.height = adHeight;
		} else {
			img.className += "rtb_network_logo";
		}

		domElement.appendChild(img);
	}

	//// RTB1 ////
	dom.rtb1_box.style.color = data.rtb1_color;
	dom.rtb1_headline.innerHTML = data.rtb1_headline;
	fitText(dom.rtb1_headline);
	dom.rtb1_title.innerHTML = data.rtb1_title;
	fitText(dom.rtb1_title);

	loadImg(data.rtb1_image.Url, dom.rtb1_img);
	loadImg(data.rtb1_network_logo.Url, dom.rtb1_network_logo);

	//// RTB2 ////
	dom.rtb2_box.style.color = data.rtb2_color;
	dom.rtb2_headline.innerHTML = data.rtb2_headline;
	fitText(dom.rtb2_headline);
	dom.rtb2_title.innerHTML = data.rtb2_title;
	fitText(dom.rtb2_title);

	loadImg(data.rtb2_image.Url, dom.rtb2_img);
	loadImg(data.rtb2_network_logo.Url, dom.rtb2_network_logo);

	//// KEY MESSAGE ////
	dom.key_message_txt.innerHTML = data.key_message_headline;
	fitText(dom.key_message_txt);

	//// CTA ////
	var ctas = document.getElementsByClassName("cta");
	for ( var i = 0; i < ctas.length; i++ ) {
		ctas[i].innerHTML = data.cta;
	}

	//// LEGAl ////
	dom.legal.innerHTML = data.legal;

	//// EXIT_URL ////
	exit_url = data.exit_url.Url;
	clickThrough();
	rollover();

	// Check if all images in the dom have been loaded before playing the master timeline.

	Promise.all([].slice.call(document.images).map(img => {
		if ( img.complete ) {
			if ( img.naturalHeight !== 0 ) {

				return Promise.resolve();
			} else return Promise.reject(img);
		}
		return new Promise((resolve, reject) => {
			img.addEventListener('load', resolve);
			img.addEventListener('error', () => reject(img));
		});
	})).then(() => {
		master_animation().play(); /// IF ALL IMAGES LOADED, MASTER ANIMATION WILL START PLAYING
	}, () => {
		dom.backup.classList.add('backup');
	});

};
