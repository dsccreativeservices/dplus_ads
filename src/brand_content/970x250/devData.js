export const getData = mode => {
	Enabler.setProfileId(10584010); /// DYNAMIC PROFILE ID
	if ( mode === 'live' ) {
		return dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0];
	} else {
		var devDynamicContent = {};

		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES = [{}];
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0]._id = 0;
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Unique_ID = "-10ifdy";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Reporting_Label = "Dplus^Launch^Dynamic^300x250^Dynamic^Content^start_free_trial^Lifestyle_Lifestyle^NA^NA^Deep_Dive_Lifestyle_1^Long_Island_Medium^My_Big_Fat_Fabulous_Life";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date.RawValue = "01/04/2021 00:00 (-05:00)";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date.UtcValue = 1609736400000;
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date.RawValue = "02/14/2021 23:59 (-05:00)";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date.UtcValue = 1613365140000;
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Dynamic_Targeting_Key = "";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].silo_name = "Content";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_genre = "Lifestyle";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_headline = "Stream all the tears.";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_title = "Long Island Medium";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_color = "#a90d39";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image.Type = "file";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201202094811051_Property_Brothers__Celebrity_IOU--HGTV--aqua--970x250.jpg";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo.Type = "file";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201118172518502_logo-TLC.svg";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_genre = "Lifestyle";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_headline = "Stream real life.";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_title = "My Big Fat Fabulous Life";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_color = "#a90d39";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image.Type = "file";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201202094733029_David_Tomborowsky_and_Annie_Suwan__90_Day_Fiance_Universe--TLC--red--970x250.jpg";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo.Type = "file";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201118172458718_logo-DSC.svg";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].key_message_id = "Deep Dive Lifestyle 1";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].key_message_headline = "Stream life-changing moments. All in one place.";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].cta = "start free trial";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].legal = "Terms Apply.";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].exit_url = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].exit_url.Url = "https://www.discoveryplus.com/";

		Enabler.setDevDynamicContent(devDynamicContent);

		return devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0];
	}
};
