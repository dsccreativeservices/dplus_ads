export const getData = mode => {
	Enabler.setProfileId(10588746); /// DYNAMIC PROFILE ID
	if ( mode === 'live' ) {
		return dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0];
	} else {
		var devDynamicContent = {};

		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES = [{}];
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0]._id = 0;
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].Unique_ID = "-15iml";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].Reporting_Label = "Dplus^Launch^Dynamic^160x600^Dynamic^Value^stream_now^NA^NA^NA^7Day_Trial_1^Paula_Zahn^Property_Brothers";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].Start_Date = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].Start_Date.RawValue = "01/04/2021 00:00 (-05:00)";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].Start_Date.UtcValue = 1609736400000;
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].End_Date = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].End_Date.RawValue = "02/14/2021 23:59 (-05:00)";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].End_Date.UtcValue = 1613365140000;
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].Dynamic_Targeting_Key = "";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].silo_name = "Value";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].key_message_id = "7Day Trial 1";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].key_message_headline = "Start your 7-day free trial now.";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].value_image = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].value_image.Type = "file";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].value_image.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201211122339126_Paula_Zahn__Property_Brothers--160x600.jpg";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].cta = "stream now";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].legal = "Terms Apply.";
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].exit_url = {};
		devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0].exit_url.Url = "https://www.discoveryplus.com/";
		Enabler.setDevDynamicContent(devDynamicContent);

		return devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__ALL_SIZES[0];
	}
};
