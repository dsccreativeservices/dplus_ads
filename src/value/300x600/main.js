import { es5, domIds } from 'banner-utils';
import { fitText } from 'dynamic-utils';
import {getData} from "./getData.js";


const Banner = {
    init: () => {

        const dom = domIds();
        const adWidth = 300;
        // const adHeight = 600;
        const mode = 'dev';
        let exit_url = "";

        // Display the banner
        es5() ? display() : dom.backup.classList.add('backup');

        function display() {
            // Setup -------------------------------------------------
            /**
             * Parses incoming dynamic data
             */
            function parseData() {
                Enabler.setProfileId(10584193); /// DYNAMIC PROFILE ID

                const data = getData(mode); // load dynamic data - set to 'dev' for local testing and 'live' before uploading to Studio
                /**                               // set value for value template
                 * Fill all HTML elements with parsed data
                 */
                function populateFrames() {
                    // Create and append an image
                    function loadImg(imgUrl, domElement) {
                        var img = document.createElement('img');
                        img.src = imgUrl;
                        img.width = adWidth;
                        domElement.appendChild(img);
                    }

                    /// PROMO IMAGE
                    loadImg(data.value_image.Url, dom.value_image);

                    //// HEADLINE ////
                    dom.key_message_headline.innerHTML = data.key_message_headline;
                    fitText(dom.key_message_headline);

                    //// CTA ////
                    dom.cta.innerHTML = data.cta;

                    //// LEGAl ////
                    dom.legal.innerHTML = data.legal;

                    //// EXIT_URL ////
                    exit_url = data.exit_url.Url;
                    clickThrough();
                    rollover();

                    // Check if all images in the dom have been loaded before showing the ad.

                    Promise.all([].slice.call(document.images).map(img => {
                        if (img.complete) {
                            if (img.naturalHeight !== 0) {
                                return Promise.resolve();
                            }
                            else return Promise.reject(img);
                        }
                        return new Promise((resolve, reject) => {
                            img.addEventListener('load', resolve);
                            img.addEventListener('error', () => reject(img));
                        });
                    })).then(() => {
                        dom.ad_content.classList.remove('invisible');
                    }, () => {
                        dom.backup.classList.add('backup');
                    });
                }
                /////// END POPULATE FRAMES //////
                // Add all dynamic content to each frame
                populateFrames();
            }
            /////// END PARSE //////

            /**
             * Check if page is loaded
             */
            if (Enabler.isPageLoaded()) parseData();
            else Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, parseData);


            // Events ------------------------------------------------

            function rollover() {
                dom.ad_content.addEventListener('mouseenter', () => {
                    gsap.set("#cta",{background:"linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 70%)"});
                });

                dom.ad_content.addEventListener('mouseleave', () => {
                    gsap.set("#cta",{background:"linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 100%)"});
                });
            }

            function clickThrough() {
                dom.ad_content.addEventListener('click', () => Enabler.exitOverride("main exit",exit_url));
            }
        }
    }
};

window.onload = () => {
    if (Enabler.isInitialized()) Banner.init();
    else Enabler.addEventListener(studio.events.StudioEvent.INIT, Banner.init);
};
