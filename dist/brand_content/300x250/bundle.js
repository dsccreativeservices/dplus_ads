(function () {
  'use strict';

  // BannerUtils version 3.2.0
  function getBrowser() {
    // desktop browsers as of 2019-10-04
    var browserslist = ['other', 'blink', 'chrome', 'safari', 'opera', 'ie', 'edge', 'firefox'];
    var browser = 0;

    if ('WebkitAppearance' in document.documentElement.style) {
      browser = 1; // chrome/safari/opera/edge/firefox

      if (/google/i.test(window.navigator.vendor)) browser = 2;
      if (/apple/i.test(window.navigator.vendor)) browser = 3;
      if (!!window.opr && !!window.opr.addons || !!window.opera || / OPR\//.test(window.navigator.userAgent)) browser = 4;
    }

    if (
    /*@cc_on!@*/
     !!document.documentMode) browser = 5; // ie 6-11

    if (browser !== 5 && !!window.StyleMedia) browser = 6;
    if (typeof InstallTrigger !== 'undefined' || 'MozAppearance' in document.documentElement.style) browser = 7;
    return browserslist[browser];
  }
  var browser = getBrowser();
  function es5() {
    return parseInt('010', 10) === 10 && function () {
      return !this;
    }() && !!(Date && Date.prototype && Date.prototype.toISOString); // IE10, FF21, CH23, SF6, OP15, iOS7, AN4.4
  }
  var log = {
    // https://bit.ly/32ZIpgo
    traceOn: window.console.log.bind(window.console, '%s'),
    traceOff: function traceOff() {},
    trace: window.console.log.bind(window.console, '%s'),

    set debug(bool) {
      this._debug = bool;
      bool ? this.trace = this.traceOn : this.trace = this.traceOff;
    },

    get debug() {
      return this._debug;
    }

  };
  function domIds(scope) {
    if (scope === void 0) {
      scope = document;
    }

    var all = scope.getElementsByTagName('*');
    var haveIds = {};
    var i = all.length;

    while (i--) {
      if (all[i].id) {
        var safeId = all[i].id.replace(/-|:|\./g, '_');
        haveIds[safeId] = all[i];
      }
    }

    return haveIds;
  }

  var getData = function getData(mode) {
    Enabler.setProfileId(10586297);

    if (mode === 'live') {
      return dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0];
    } else {
      var devDynamicContent = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES = [{}];
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0]._id = 0;
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Unique_ID = "-10ifdy";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Reporting_Label = "Dplus^Launch^Dynamic^300x250^Dynamic^Content^start_free_trial^Lifestyle_Lifestyle^NA^NA^Deep_Dive_Lifestyle_1^Long_Island_Medium^My_Big_Fat_Fabulous_Life";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date.RawValue = "01/04/2021 00:00 (-05:00)";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date.UtcValue = 1609736400000;
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date.RawValue = "02/14/2021 23:59 (-05:00)";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date.UtcValue = 1613365140000;
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Dynamic_Targeting_Key = "";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].silo_name = "Content";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_genre = "Lifestyle";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_headline = "Stream all the tears.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_title = "Long Island Medium";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_color = "#ff0000";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201202094811051_Property_Brothers__Celebrity_IOU--HGTV--aqua--970x250.jpg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201118172518502_logo-TLC.svg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_genre = "Lifestyle";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_headline = "Stream real life.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_title = "My Big Fat Fabulous Life";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_color = "#ff0000";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201202094733029_David_Tomborowsky_and_Annie_Suwan__90_Day_Fiance_Universe--TLC--red--970x250.jpg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo.Url = "https://s0.2mdn.net/preview/CgkIARC45MWe7i4SGQCkoPPI9F2hgjOF1ZksX3NP0m7_FaLnXU4/ads/richmedia/studio/22921646/__version__/7/22921646_20210105070600310_logo-BBC-PP.svg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].key_message_id = "Deep Dive Lifestyle 1";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].key_message_headline = "Stream life-changing moments. All in one place.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].cta = "start free trial";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].legal = "Terms Apply.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].exit_url = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].exit_url.Url = "https://www.discoveryplus.com/";
      Enabler.setDevDynamicContent(devDynamicContent);
      return devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0];
    }
  };

  // Version 5.0.4
  function fitText(elem, cb, args) {
    var isOverflowing = function isOverflowing() {
      return elem.scrollWidth > elem.offsetWidth || elem.scrollHeight > elem.offsetHeight;
    };

    var overStyle = elem.style.overflow;
    if (!overStyle || overStyle === 'visible') elem.style.overflow = 'hidden';

    function finish() {
      elem.style.overflow = overStyle;
      if (cb) args ? cb.apply(this, args) : cb.apply(this);
      elem.style.height = "auto"; //// fits box to real text height
    }

    function checkSize() {
      var fontSize = parseFloat(window.getComputedStyle(elem).fontSize, 10);

      if (isOverflowing() && fontSize > 7) {
        fontSize % 1 !== 0 ? fontSize = Math.floor(fontSize) : fontSize--;
        elem.style.fontSize = fontSize + "px";
        setTimeout(checkSize, 10); // WARN: async callback
      } else finish();
    }

    checkSize();
  }

  var exit_url = "";
  var dom = domIds();

  function rollover() {
    dom.ad_content.addEventListener('mouseenter', function () {
      gsap.set(".cta", {
        background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 70%)"
      });
    });
    dom.ad_content.addEventListener('mouseleave', function () {
      gsap.set(".cta", {
        background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 100%)"
      });
    });
  }

  function clickThrough() {
    // dom.ad_content.addEventListener('click', () => Enabler.exitOverride("main exit", exit_url));
    dom.ad_content.addEventListener('click', function () {
      return Enabler.exit("main exit");
    });
  }

  var populateFrames = (function (adWidth, data, master_animation, transforms) {
    if (transforms === void 0) {
      transforms = null;
    }

    // Create and append an image
    function loadImg(imgUrl, domElement) {
      var fileType = imgUrl.substring(imgUrl.length - 3);
      var img = document.createElement('img');
      img.src = imgUrl;

      if (fileType === "jpg") {
        if (transforms) {
          transforms(img);
        } else {
          img.width = adWidth;
        } // img.height = adHeight;

      } else {
        img.className += "rtb_network_logo";
      }

      domElement.appendChild(img);
    } //// RTB1 ////


    dom.rtb1_box.style.color = data.rtb1_color;
    dom.rtb1_headline.innerHTML = data.rtb1_headline;
    fitText(dom.rtb1_headline);
    dom.rtb1_title.innerHTML = data.rtb1_title;
    fitText(dom.rtb1_title);
    loadImg(data.rtb1_image.Url, dom.rtb1_img);
    loadImg(data.rtb1_network_logo.Url, dom.rtb1_network_logo); //// RTB2 ////

    dom.rtb2_box.style.color = data.rtb2_color;
    dom.rtb2_headline.innerHTML = data.rtb2_headline;
    fitText(dom.rtb2_headline);
    dom.rtb2_title.innerHTML = data.rtb2_title;
    fitText(dom.rtb2_title);
    loadImg(data.rtb2_image.Url, dom.rtb2_img);
    loadImg(data.rtb2_network_logo.Url, dom.rtb2_network_logo); //// KEY MESSAGE ////

    dom.key_message_txt.innerHTML = data.key_message_headline;
    fitText(dom.key_message_txt); //// CTA ////

    var ctas = document.getElementsByClassName("cta");

    for (var i = 0; i < ctas.length; i++) {
      ctas[i].innerHTML = data.cta;
    } //// LEGAl ////


    dom.legal.innerHTML = data.legal; //// EXIT_URL ////

    exit_url = data.exit_url.Url;
    clickThrough();
    rollover(); // Check if all images in the dom have been loaded before playing the master timeline.

    Promise.all([].slice.call(document.images).map(function (img) {
      if (img.complete) {
        if (img.naturalHeight !== 0) {
          return Promise.resolve();
        } else return Promise.reject(img);
      }

      return new Promise(function (resolve, reject) {
        img.addEventListener('load', resolve);
        img.addEventListener('error', function () {
          return reject(img);
        });
      });
    })).then(function () {
      master_animation().play(); /// IF ALL IMAGES LOADED, MASTER ANIMATION WILL START PLAYING
    }, function () {
      dom.backup.classList.add('backup');
    });
  });

  var Banner = {
    init: function init() {
      var dom = domIds();
      var adWidth = 300,
          adHeight = 250;
   // Display the banner

      es5() ? display() : dom.backup.classList.add('backup');

      function display() {
        // Setup -------------------------------------------------

        /**
         * Parses incoming dynamic data
         */
        function parseData() {
          var data = getData('dev');
          populateFrames(adWidth, data, master_animation, function (img) {
            img.height = adHeight;
          });
        } /////// END PARSE //////

        /**
         * Main banner animations
         */


        function endFrameArrows() {
          var tl = gsap.timeline({
            defaults: {
              duration: 0.25,
              ease: 'power3.inOut'
            }
          });
          tl.to("#arrow_wrap", 0.5, {
            x: 0,
            y: 0,
            scale: 0.95
          }, "-=0.5");
          return tl;
        }

        function centerArrows() {
          var tl = gsap.timeline({
            defaults: {
              duration: 0.25,
              ease: 'power3.inOut'
            }
          });
          tl.add("anim").to("#arrow1", {
            x: 0,
            y: 0
          }, "anim").to("#arrow2", {
            x: 0,
            y: 0
          }, "anim");
          return tl;
        }

        function openFrame(wrapper, image) {
          var tl = gsap.timeline({
            defaults: {
              duration: 0.5,
              ease: 'power3.inOut'
            }
          });
          tl.set(wrapper, {
            autoAlpha: 1
          }).add("anim").to("#arrow1", {
            x: -144,
            y: -117
          }, "anim").to("#arrow2", {
            x: 141,
            y: 118
          }, "anim").from(wrapper, {
            width: 0,
            height: 0,
            x: adWidth / 2,
            y: adHeight / 2,
            force3D: false
          }, "anim").from(image, {
            x: -adWidth / 2,
            y: -adHeight / 2,
            force3D: false
          }, "anim");
          return tl;
        }

        function closeFrame(wrapper, image) {
          var tl = gsap.timeline({
            defaults: {
              duration: 0.5,
              ease: 'power3.inOut'
            }
          });
          tl.set(wrapper, {
            autoAlpha: 1
          }).add("anim").to("#arrow1", {
            x: 0,
            y: 0
          }, "anim").to("#arrow2", {
            x: 0,
            y: 0
          }, "anim").to(wrapper, {
            width: 0,
            height: 0,
            x: adWidth / 2,
            y: adHeight / 2,
            force3D: false
          }, "anim").to(image, {
            x: -adWidth / 2,
            y: -adHeight / 2,
            force3D: false
          }, "anim");
          return tl;
        }

        function f1_animation() {
          dom.ad_content.classList.remove('invisible');
          var tl = gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          });
          tl.to(["#logo", ".cta_f1"], 0.5, {
            scale: 0.95,
            autoAlpha: 0
          }, "+=1").to("#arrow_wrap", 0.75, {
            x: -75,
            y: -8,
            scale: 1
          }, "-=0.5").to(".arrow", 0.75, {
            fill: "#FFFFFF"
          }, "-=0.75");
          return tl;
        }

        function f2_animation() {
          var tl = gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          });
          tl.add(openFrame("#rtb1_mask", "#rtb1_wrap")).add(centerArrows(), "+=3.5");
          return tl;
        }

        function f3_animation() {
          var tl = gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          });
          tl.set("#key_message_cta", {
            autoAlpha: 1
          }).add(openFrame("#rtb2_mask", "#rtb2_wrap")).add(closeFrame("#rtb2_mask", "#rtb2_wrap"), "+=3.5").set(".logo_small_grey", {
            autoAlpha: 1
          }, "-=0.35").set("#rtb1_mask", {
            autoAlpha: 0
          }, "-=1");
          return tl;
        }

        function f4_animation() {
          var tl = gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          });
          tl.add(openFrame("#key_message_mask", ".key_message_wrap")) // .addPause()
          .to(".arrow", 0.75, {
            fill: "#4C4F55"
          }, "-=0.5").add(closeFrame("#key_message_mask", ".key_message_wrap"), "+=3").set(".logo_small_grey,#key_message_cta", {
            autoAlpha: 0
          }, "-=0.25");
          return tl;
        }

        function ef_animation() {
          var tl = gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          });
          tl.add(endFrameArrows()).to(["#logo", ".cta_f1"], 0.5, {
            scale: 1,
            autoAlpha: 1
          }, "-=0.5"); // .from(["#ef_logo,#cta,#ef_logos"],0.25,{autoAlpha:0,stagger:0.1})

          return tl;
        }

        function master_animation() {
          var tl = gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          });
          tl.add(f1_animation()).add(f2_animation()) // .addPause()
          .add(f3_animation()).add(f4_animation()) // .addPause()
          .add(ef_animation());
          return tl;
        }
        /**
         * Check if page is loaded
         */


        if (Enabler.isPageLoaded()) parseData();else Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, parseData); // Events ------------------------------------------------
      }
    }
  };

  window.onload = function () {
    if (Enabler.isInitialized()) Banner.init();else Enabler.addEventListener(studio.events.StudioEvent.INIT, Banner.init);
  };

}());
