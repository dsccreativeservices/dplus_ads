(function () {
  'use strict';

  // BannerUtils version 3.2.0
  function getBrowser() {
    // desktop browsers as of 2019-10-04
    var browserslist = ['other', 'blink', 'chrome', 'safari', 'opera', 'ie', 'edge', 'firefox'];
    var browser = 0;

    if ('WebkitAppearance' in document.documentElement.style) {
      browser = 1; // chrome/safari/opera/edge/firefox

      if (/google/i.test(window.navigator.vendor)) browser = 2;
      if (/apple/i.test(window.navigator.vendor)) browser = 3;
      if (!!window.opr && !!window.opr.addons || !!window.opera || / OPR\//.test(window.navigator.userAgent)) browser = 4;
    }

    if (
    /*@cc_on!@*/
     !!document.documentMode) browser = 5; // ie 6-11

    if (browser !== 5 && !!window.StyleMedia) browser = 6;
    if (typeof InstallTrigger !== 'undefined' || 'MozAppearance' in document.documentElement.style) browser = 7;
    return browserslist[browser];
  }
  var browser = getBrowser();
  function es5() {
    return parseInt('010', 10) === 10 && function () {
      return !this;
    }() && !!(Date && Date.prototype && Date.prototype.toISOString); // IE10, FF21, CH23, SF6, OP15, iOS7, AN4.4
  }
  var log = {
    // https://bit.ly/32ZIpgo
    traceOn: window.console.log.bind(window.console, '%s'),
    traceOff: function traceOff() {},
    trace: window.console.log.bind(window.console, '%s'),

    set debug(bool) {
      this._debug = bool;
      bool ? this.trace = this.traceOn : this.trace = this.traceOff;
    },

    get debug() {
      return this._debug;
    }

  };
  function domIds(scope) {
    if (scope === void 0) {
      scope = document;
    }

    var all = scope.getElementsByTagName('*');
    var haveIds = {};
    var i = all.length;

    while (i--) {
      if (all[i].id) {
        var safeId = all[i].id.replace(/-|:|\./g, '_');
        haveIds[safeId] = all[i];
      }
    }

    return haveIds;
  }

  // Version 5.0.4
  function fitText(elem, cb, args) {
    var isOverflowing = function isOverflowing() {
      return elem.scrollWidth > elem.offsetWidth || elem.scrollHeight > elem.offsetHeight;
    };

    var overStyle = elem.style.overflow;
    if (!overStyle || overStyle === 'visible') elem.style.overflow = 'hidden';

    function finish() {
      elem.style.overflow = overStyle;
      if (cb) args ? cb.apply(this, args) : cb.apply(this);
      elem.style.height = "auto"; //// fits box to real text height
    }

    function checkSize() {
      var fontSize = parseFloat(window.getComputedStyle(elem).fontSize, 10);

      if (isOverflowing() && fontSize > 7) {
        fontSize % 1 !== 0 ? fontSize = Math.floor(fontSize) : fontSize--;
        elem.style.fontSize = fontSize + "px";
        setTimeout(checkSize, 10); // WARN: async callback
      } else finish();
    }

    checkSize();
  }

  var exit_url = "";
  var dom = domIds();

  function rollover() {
    dom.ad_content.addEventListener('mouseenter', function () {
      gsap.set(".cta", {
        background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 70%)"
      });
    });
    dom.ad_content.addEventListener('mouseleave', function () {
      gsap.set(".cta", {
        background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 100%)"
      });
    });
  }

  function clickThrough() {
    // dom.ad_content.addEventListener('click', () => Enabler.exitOverride("main exit", exit_url));
    dom.ad_content.addEventListener('click', function () {
      return Enabler.exit("main exit");
    });
  }

  var populateFrames = (function (adWidth, data, master_animation, transforms) {
    if (transforms === void 0) {
      transforms = null;
    }

    // Create and append an image
    function loadImg(imgUrl, domElement) {
      var fileType = imgUrl.substring(imgUrl.length - 3);
      var img = document.createElement('img');
      img.src = imgUrl;

      if (fileType === "jpg") {
        if (transforms) {
          transforms(img);
        } else {
          img.width = adWidth;
        } // img.height = adHeight;

      } else {
        img.className += "rtb_network_logo";
      }

      domElement.appendChild(img);
    } //// RTB1 ////


    dom.rtb1_box.style.color = data.rtb1_color;
    dom.rtb1_headline.innerHTML = data.rtb1_headline;
    fitText(dom.rtb1_headline);
    dom.rtb1_title.innerHTML = data.rtb1_title;
    fitText(dom.rtb1_title);
    loadImg(data.rtb1_image.Url, dom.rtb1_img);
    loadImg(data.rtb1_network_logo.Url, dom.rtb1_network_logo); //// RTB2 ////

    dom.rtb2_box.style.color = data.rtb2_color;
    dom.rtb2_headline.innerHTML = data.rtb2_headline;
    fitText(dom.rtb2_headline);
    dom.rtb2_title.innerHTML = data.rtb2_title;
    fitText(dom.rtb2_title);
    loadImg(data.rtb2_image.Url, dom.rtb2_img);
    loadImg(data.rtb2_network_logo.Url, dom.rtb2_network_logo); //// KEY MESSAGE ////

    dom.key_message_txt.innerHTML = data.key_message_headline;
    fitText(dom.key_message_txt); //// CTA ////

    var ctas = document.getElementsByClassName("cta");

    for (var i = 0; i < ctas.length; i++) {
      ctas[i].innerHTML = data.cta;
    } //// LEGAl ////


    dom.legal.innerHTML = data.legal; //// EXIT_URL ////

    exit_url = data.exit_url.Url;
    clickThrough();
    rollover(); // Check if all images in the dom have been loaded before playing the master timeline.

    Promise.all([].slice.call(document.images).map(function (img) {
      if (img.complete) {
        if (img.naturalHeight !== 0) {
          return Promise.resolve();
        } else return Promise.reject(img);
      }

      return new Promise(function (resolve, reject) {
        img.addEventListener('load', resolve);
        img.addEventListener('error', function () {
          return reject(img);
        });
      });
    })).then(function () {
      master_animation().play(); /// IF ALL IMAGES LOADED, MASTER ANIMATION WILL START PLAYING
    }, function () {
      dom.backup.classList.add('backup');
    });
  });

  var getData = function getData(mode) {
    Enabler.setProfileId(10584360); /// DYNAMIC PROFILE ID

    if (mode === 'live') {
      return dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0];
    } else {
      var devDynamicContent = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES = [{}];
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0]._id = 0;
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Unique_ID = "-10ifdy";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Reporting_Label = "Dplus^Launch^Dynamic^300x250^Dynamic^Content^start_free_trial^Lifestyle_Lifestyle^NA^NA^Deep_Dive_Lifestyle_1^Long_Island_Medium^My_Big_Fat_Fabulous_Life";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date.RawValue = "01/04/2021 00:00 (-05:00)";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Start_Date.UtcValue = 1609736400000;
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date.RawValue = "02/14/2021 23:59 (-05:00)";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].End_Date.UtcValue = 1613365140000;
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].Dynamic_Targeting_Key = "";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].silo_name = "Content";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_genre = "Lifestyle";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_headline = "Stream all the tears.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_title = "Long Island Medium";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_color = "#a90d39";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_image.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201201123441331_Diners_Drive_Ins_And_Dives--FN--yellow--320x480.jpg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb1_network_logo.Url = "https://s0.2mdn.net/preview/CgkIARC45MWe7i4SGQCkoPPI9F2hgjOF1ZksX3NP0m7_FaLnXU4/ads/richmedia/studio/22921646/__version__/7/22921646_20210105070600310_logo-BBC-PP.svg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_genre = "Lifestyle";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_headline = "Stream real life.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_title = "My Big Fat Fabulous Life";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_color = "#a90d39";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_image.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201201123438447_Deadliest_Catch--DSC--blue--320x480.jpg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo.Type = "file";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].rtb2_network_logo.Url = "https://s0.2mdn.net/ads/richmedia/studio/22921646/22921646_20201118172518502_logo-TLC.svg";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].key_message_id = "Deep Dive Lifestyle 1";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].key_message_headline = "Stream life-changing moments. All in one place.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].cta = "start free trial";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].legal = "Terms Apply.";
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].exit_url = {};
      devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0].exit_url.Url = "https://www.discoveryplus.com/";
      Enabler.setDevDynamicContent(devDynamicContent);
      return devDynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__ALL_SIZES[0];
    }
  };

  var Banner = {
    init: function init() {
      var dom = domIds();
      var adWidth = 320;
      var adHeight = 480;
      var mode = 'live';
      var arrow_wrap_initial = {
        x: 105,
        y: 20,
        scale: 1
      };
      var arrow1_corner = {
        x: -155,
        y: -236,
        scale: 0.8
      };
      var arrow2_corner = {
        x: 155,
        y: 235,
        scale: 0.8
      };

      var display = function display() {
        var openFrame = function openFrame(wrapper, image) {
          return gsap.timeline({
            defaults: {
              duration: 0.5,
              ease: 'power3.inOut'
            }
          }).set(wrapper, {
            autoAlpha: 1
          }).add("anim").to("#arrow1", arrow1_corner, "anim").to("#arrow_wrap", {
            scale: 1
          }, "anim").to("#arrow2", arrow2_corner, "anim").from(wrapper, {
            width: 0,
            height: 0,
            x: adWidth / 2,
            y: adHeight / 2,
            force3D: false
          }, "anim").from(image, {
            x: -adWidth / 2,
            y: -adHeight / 2,
            force3D: false
          }, "anim");
        };

        var centerArrows = function centerArrows() {
          return gsap.timeline({
            defaults: {
              duration: 0.5,
              ease: 'power3.inOut'
            }
          }).add("anim").to("#arrow1", {
            x: 0,
            y: 0,
            scale: 1
          }, "anim").to("#arrow2", {
            x: 0,
            y: 0,
            scale: 1
          }, "anim");
        };

        var closeFrame = function closeFrame(wrapper, image) {
          return gsap.timeline({
            defaults: {
              duration: 0.5,
              ease: 'power3.inOut'
            }
          }).set(wrapper, {
            autoAlpha: 1
          }).add(centerArrows(), "anim").to(wrapper, {
            width: 0,
            height: 0,
            x: adWidth / 2,
            y: adHeight / 2,
            force3D: false
          }, "anim").to(image, {
            x: -adWidth / 2,
            y: -adHeight / 2,
            force3D: false
          }, "anim");
        };

        var start_frame = function start_frame() {
          dom.ad_content.classList.remove('invisible');
          return gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          }).to("#arrow_wrap", 0, arrow_wrap_initial).to(["#logo", ".cta_f1"], 0.5, {
            scale: 0.95,
            autoAlpha: 0
          }, "+=1").to("#arrow_wrap", 0.75, {
            autoAlpha: 1
          }, "-=0.75").to("#arrow_wrap", 0.75, {
            x: 0,
            y: 0,
            scale: 1.3
          }, "-=0.5").to(".arrow", 0.75, {
            fill: "#FFFFFF"
          }, "-=0.75");
        };

        var rtb1_frame = function rtb1_frame() {
          return gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          }).add(openFrame("#middle_frames", "#middle_frames_inner")).add(centerArrows(), "+=3.5");
        };

        var rtb2_frame = function rtb2_frame() {
          return gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          }).set("#key_message_cta", {
            autoAlpha: 1
          }).add(openFrame("#rtb2_mask", "#rtb2_wrap")).add(closeFrame("#rtb_frames", "#rtb_frames_inner"), "+=3.5").set(".logo_small_grey", {
            autoAlpha: 1
          }, "-=0.35");
        };

        var key_message_frame = function key_message_frame() {
          return gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          }).add(openFrame("#key_message_mask", ".key_message_wrap")).to(".arrow", 0.75, {
            fill: "#4C4F55"
          }, "-=0.5").add(closeFrame("#middle_frames", "#middle_frames_inner"), "+=3").set(".logo_small_grey,#key_message_cta", {
            autoAlpha: 0
          }, "-=0.25");
        };

        var end_frame = function end_frame() {
          var endFrameArrows = function endFrameArrows() {
            return gsap.timeline({
              defaults: {
                duration: 0.25,
                ease: 'power3.inOut'
              }
            }).to("#arrow_wrap", 0.5, arrow_wrap_initial, "-=0.5").to(["#arrow1", "#arrow2"], {
              scale: 1
            }, "-=0.5");
          };

          return gsap.timeline({
            defaults: {
              ease: 'expo.inOut'
            }
          }).add(endFrameArrows()).to(["#logo", ".cta_f1"], 0.5, {
            scale: 1,
            autoAlpha: 1
          }, "-=0.5").to("#arrow_wrap", 0.25, {
            autoAlpha: 0
          }, "-=0.25");
        };

        var master_animation = function master_animation() {
          return gsap.timeline().add(start_frame()).add(rtb1_frame()).add(rtb2_frame()).add(key_message_frame()) // .addPause()
          .add(end_frame());
        };

        var parseData = function parseData() {
          var data = getData(mode);
          populateFrames(adWidth, data, master_animation, function (img) {
            img.width = adWidth;
          });
        };

        if (Enabler.isPageLoaded()) {
          parseData();
        } else {
          Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, parseData);
        }
      }; // Display the banner


      es5() ? display() : dom.backup.classList.add('backup');
    }
  };

  window.onload = function () {
    if (Enabler.isInitialized()) Banner.init();else Enabler.addEventListener(studio.events.StudioEvent.INIT, Banner.init);
  };

}());
