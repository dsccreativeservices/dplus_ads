(function () {
  'use strict';

  // BannerUtils version 3.2.0
  function getBrowser() {
    // desktop browsers as of 2019-10-04
    var browserslist = ['other', 'blink', 'chrome', 'safari', 'opera', 'ie', 'edge', 'firefox'];
    var browser = 0;

    if ('WebkitAppearance' in document.documentElement.style) {
      browser = 1; // chrome/safari/opera/edge/firefox

      if (/google/i.test(window.navigator.vendor)) browser = 2;
      if (/apple/i.test(window.navigator.vendor)) browser = 3;
      if (!!window.opr && !!window.opr.addons || !!window.opera || / OPR\//.test(window.navigator.userAgent)) browser = 4;
    }

    if (
    /*@cc_on!@*/
     !!document.documentMode) browser = 5; // ie 6-11

    if (browser !== 5 && !!window.StyleMedia) browser = 6;
    if (typeof InstallTrigger !== 'undefined' || 'MozAppearance' in document.documentElement.style) browser = 7;
    return browserslist[browser];
  }
  var browser = getBrowser();
  function es5() {
    return parseInt('010', 10) === 10 && function () {
      return !this;
    }() && !!(Date && Date.prototype && Date.prototype.toISOString); // IE10, FF21, CH23, SF6, OP15, iOS7, AN4.4
  }
  var log = {
    // https://bit.ly/32ZIpgo
    traceOn: window.console.log.bind(window.console, '%s'),
    traceOff: function traceOff() {},
    trace: window.console.log.bind(window.console, '%s'),

    set debug(bool) {
      this._debug = bool;
      bool ? this.trace = this.traceOn : this.trace = this.traceOff;
    },

    get debug() {
      return this._debug;
    }

  };
  function domIds(scope) {
    if (scope === void 0) {
      scope = document;
    }

    var all = scope.getElementsByTagName('*');
    var haveIds = {};
    var i = all.length;

    while (i--) {
      if (all[i].id) {
        var safeId = all[i].id.replace(/-|:|\./g, '_');
        haveIds[safeId] = all[i];
      }
    }

    return haveIds;
  }

  function data300x250 (mode, template) {
    var dimensions = '300x250';
    var data;
    var devDynamicContent = {};

    switch (mode) {
      case 'live':
        // dynamicContent is automatically set by the Studio enabler if the ad is in Studio and dynamic data has loaded
        // it is only available after the StudioEvent.INIT event is fired (Banner.init is only called after StudioEvent.INIT, so no problem there)
        // data = dynamicContent ? dynamicContent.Profile[0] : data;
        if (template === "brand_content") {
          //// BRAND CONTENT DATA
          data = dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__300x250[0]; //// THIS IS THE SHEET NAME YOU IMPORTED TO YOUR STUDIO'S PROFILE, SO MAKE SURE YOU ARE USING THE SAME NAME
        } else {
          //// VALUE DATA
          data = dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__300x250[0]; //// THIS IS THE SHEET NAME YOU IMPORTED TO YOUR STUDIO'S PROFILE, SO MAKE SURE YOU ARE USING THE SAME NAME
        }

        break;

      case 'dev':
        if (template === "brand_content") {
          ///// SILO 1 & 2 TESTING ENVIRONMENT
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2 = [{}];
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0]._id = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Unique_ID = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Reporting_Label = "test"; //// RTB 1 ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_headline = "Fromzz wildlife spectacles";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_title = "Planet Earth";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_color = "#FFFFFF";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_image = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_image.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_image.Url = "localassets/rtb1_" + dimensions + ".jpg";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_network_logo = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_network_logo.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_network_logo.Url = "localassets/logo-BBC-PE1.svg"; //// RTB 2 ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_headline = "To close animal encounters.";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_title = "Planet Earth II";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_color = "#fff";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_image = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_image.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_image.Url = "localassets/rtb2_" + dimensions + ".jpg";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_network_logo = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_network_logo.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_network_logo.Url = "localassets/logo-BBC-PE2.svg"; //// KEY MESSAGE ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].key_message_headline = "The only place to stream BBC's Planet Earth."; //// CTA ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].cta = "start free trial"; //// LEGAl ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].legal = "Terms Apply."; //// EXIT_URL ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].exit_url = "http://www.discovery.com"; // Initializes the enabler with the test content

          Enabler.setDevDynamicContent(devDynamicContent); // data = devDynamicContent.Profile[0];

          data = devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0];
        } else {
          ///// VALUE TESTING ENVIRONMENT
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2 = [{}];
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0]._id = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Unique_ID = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Reporting_Label = "test"; //// HEADLINE ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].key_message_headline = "stream what you love";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].value_image = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].value_image.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].value_image.Url = "localassets/value_img_" + dimensions + ".jpg"; //// CTA ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].cta = "stream now"; //// LEGAl ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].legal = "terms apply"; //// EXIT_URL ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].exit_url = "http://www.discovery.com"; // Initializes the enabler with the test content

          Enabler.setDevDynamicContent(devDynamicContent);
          data = devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0];
        }

        break;
    }

    return data;
  }

  function data160x600 (mode, template) {
    var dimensions = '160x600';
    var data;
    var devDynamicContent = {};

    switch (mode) {
      case 'live':
        // dynamicContent is automatically set by the Studio enabler if the ad is in Studio and dynamic data has loaded
        // it is only available after the StudioEvent.INIT event is fired (Banner.init is only called after StudioEvent.INIT, so no problem there)
        // data = dynamicContent ? dynamicContent.Profile[0] : data;
        if (template === "brand_content") {
          //// BRAND CONTENT DATA
          data = dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Brand__Content__160x600[0]; //// THIS IS THE SHEET NAME YOU IMPORTED TO YOUR STUDIO'S PROFILE, SO MAKE SURE YOU ARE USING THE SAME NAME
        } else {
          //// VALUE DATA
          data = dynamicContent.CLIENT_discovery_Google_Studio_Feeds_Value__160x600[0]; //// THIS IS THE SHEET NAME YOU IMPORTED TO YOUR STUDIO'S PROFILE, SO MAKE SURE YOU ARE USING THE SAME NAME
        }

        break;

      case 'dev':
        if (template === "brand_content") {
          ///// SILO 1 & 2 TESTING ENVIRONMENT
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2 = [{}];
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0]._id = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Unique_ID = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Reporting_Label = "test"; //// RTB 1 ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_headline = "From wildlife spectacles";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_title = "Planet Earth";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_color = "#FFFFFF";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_image = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_image.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_image.Url = "localassets/rtb1_" + dimensions + ".jpg";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_network_logo = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_network_logo.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb1_network_logo.Url = "localassets/logo-BBC-PE1.svg"; //// RTB 2 ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_headline = "To close animal encounters.";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_title = "Planet Earth II";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_color = "#fff";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_image = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_image.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_image.Url = "localassets/rtb2_" + dimensions + ".jpg";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_network_logo = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_network_logo.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].rtb2_network_logo.Url = "localassets/logo-BBC-PE2.svg"; //// KEY MESSAGE ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].key_message_headline = "The only place to stream BBC's Planet Earth."; //// CTA ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].cta = "start free trial"; //// LEGAl ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].legal = "Terms Apply."; //// EXIT_URL ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].exit_url = "http://www.discovery.com"; // Initializes the enabler with the test content

          Enabler.setDevDynamicContent(devDynamicContent); // data = devDynamicContent.Profile[0];

          data = devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0];
        } else {
          ///// VALUE TESTING ENVIRONMENT
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2 = [{}];
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0]._id = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Unique_ID = 0;
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].Reporting_Label = "test"; //// HEADLINE ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].key_message_headline = "stream what you love";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].value_image = {};
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].value_image.Type = "file";
          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].value_image.Url = "localassets/value_img_" + dimensions + ".jpg"; //// CTA ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].cta = "stream now"; //// LEGAl ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].legal = "terms apply"; //// EXIT_URL ////

          devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0].exit_url = "http://www.discovery.com"; // Initializes the enabler with the test content

          Enabler.setDevDynamicContent(devDynamicContent);
          data = devDynamicContent.Discovey_Plus__Dynamic_Feed_Sheet2[0];
        }

        break;
    }

    return data;
  }

  /**
   * Gets dynamic data
   *
   * @param {String} mode Indicates whether to return test data or full dynamic data.
   * Accepts two values, 'live' or 'dev'. dev will return local test data. 'live' will return true dynamic data,
   * but will only work if the banner is uploaded to Studio.
   * @param {string} template
   * @param {string} size
   */
  function getDynamicData(mode, template, size) {
    return {
      '300x250': data300x250,
      '160x600': data160x600
    }[size](mode, template);
  }

  // Version 5.0.4
  function fitText(elem, cb, args) {
    var isOverflowing = function isOverflowing() {
      return elem.scrollWidth > elem.offsetWidth || elem.scrollHeight > elem.offsetHeight;
    };

    var overStyle = elem.style.overflow;
    if (!overStyle || overStyle === 'visible') elem.style.overflow = 'hidden';

    function finish() {
      elem.style.overflow = overStyle;
      if (cb) args ? cb.apply(this, args) : cb.apply(this);
      elem.style.height = "auto"; //// fits box to real text height
    }

    function checkSize() {
      var fontSize = parseFloat(window.getComputedStyle(elem).fontSize, 10);

      if (isOverflowing() && fontSize > 7) {
        fontSize % 1 !== 0 ? fontSize = Math.floor(fontSize) : fontSize--;
        elem.style.fontSize = fontSize + "px";
        setTimeout(checkSize, 10); // WARN: async callback
      } else finish();
    }

    checkSize();
  }

  var Banner = {
    init: function init() {
      var dom = domIds();
      var adWidth = 300,
          adHeight = 250,
          exit_url = ""; // Display the banner

      es5() ? display() : dom.backup.classList.add('backup');

      function display() {
        // Setup -------------------------------------------------

        /**
         * Parses incoming dynamic data
         */
        function parseData() {
          Enabler.setProfileId(10581027); /// 300x250

          var data = getDynamicData('dev', "value", adWidth + "x" + adHeight); // load dynamic data - set to 'dev' for local testing and 'live' before uploading to Studio

          /**                               // set value for value template
          * Fill all HTML elements with parsed data
          */

          function populateFrames() {
            // Create and append an image
            function loadImg(imgUrl, domElement) {
              var img = document.createElement('img');
              img.src = imgUrl;
              img.width = adWidth;
              domElement.appendChild(img);
            } /// PROMO IMAGE


            loadImg(data.value_image.Url, dom.value_image); //// HEADLINE ////

            dom.key_message_headline.innerHTML = data.key_message_headline;
            fitText(dom.key_message_headline); //// CTA ////

            dom.cta.innerHTML = data.cta; //// LEGAl ////

            dom.legal.innerHTML = data.legal; //// EXIT_URL ////

            exit_url = data.exit_url.Url;
            clickThrough();
            rollover(); // Check if all images in the dom have been loaded before showing the ad.

            Promise.all([].slice.call(document.images).map(function (img) {
              if (img.complete) {
                if (img.naturalHeight !== 0) {
                  return Promise.resolve();
                } else return Promise.reject(img);
              }

              return new Promise(function (resolve, reject) {
                img.addEventListener('load', resolve);
                img.addEventListener('error', function () {
                  return reject(img);
                });
              });
            })).then(function () {
              dom.ad_content.classList.remove('invisible');
            }, function () {
              dom.backup.classList.add('backup');
            });
          } /////// END POPULATE FRAMES //////
          // Add all dynamic content to each frame


          populateFrames();
        } /////// END PARSE //////

        /**
         * Check if page is loaded
         */


        if (Enabler.isPageLoaded()) parseData();else Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, parseData); // Events ------------------------------------------------

        function rollover() {
          dom.ad_content.addEventListener('mouseenter', function () {
            gsap.set("#cta", {
              background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 70%)"
            });
          });
          dom.ad_content.addEventListener('mouseleave', function () {
            gsap.set("#cta", {
              background: "linear-gradient(180deg, rgba(107,227,232,1) 0%, rgba(55,165,183,1) 100%)"
            });
          });
        }

        function clickThrough() {
          dom.ad_content.addEventListener('click', function () {
            return Enabler.exitOverride("main exit", exit_url);
          });
        }
      }
    }
  };

  window.onload = function () {
    if (Enabler.isInitialized()) Banner.init();else Enabler.addEventListener(studio.events.StudioEvent.INIT, Banner.init);
  };

}());
