# The HTML Banner Template

Starter template for HTML5 banner development.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for work production, internal development and testing purposes.

### Prerequisites

In order to develop your banners you need to meet these requirements:

* macOS Sierra or later & command-line tools for XCode.
* Windows 10 latest update possible
* [NodeJS & NPM](https://nodejs.org/)
* [Git](https://git-scm.com/downloads)

 If you need help installing the software, you can refer to the Setup Guide [here](docs/setup-guide.md).

## Downloading

If you plan on using this template as a starter project, you should download a [ZIP file](https://github.com/aDunhamDSC/dplus_ads/archive/master.zip) from the GitHub project and initialize a new Git repo for a fresh start.

## Usage

Open your terminal and navigate to the folder containing package.json. Ideally there shouldn't be a node_modules folder. Proceed to install them by typing:

    npm i

After the node_modules are installed, run the start script to initialize a local server and file watchers:

    npm start

You can make a showcase page of all the banners by running:

    npm run index

When ready for deployment, zip all files to make the deliverables your team needs by running:

    npm run zip

This task will generate the showcase page automatically.

## File Structure

NPM Scripts call CLI commands or local JS task files (./lib/tasks).  
This project uses a single *dist* and *src* folder. With the exception of certain binary files like videos or fonts, nothing in the *dist* should need to be modified. When running, most files deleted from *src* will also be deleted from *dist*, but some old/unused files may need to be manually deleted from *dist*.

Files to edit inside *src* folder:

    index.html
    main.scss
    main.js

Folders to drop images into for sprite output inside *src* folder:

    styles/sprite-mov (PNG or JPG files for image sequence animations)
    styles/sprite-svg (only SVG files)
    styles/sprite-png (only PNG files @1x, @1.5x @2x, @0.5x)
    styles/sprite-jpg (JPG, PNG or GIF files @1x, @1.5x @2x, @0.5x (for JPG output))
    
Targeting images from sprite folder on your HTML

    styles/sprite-png/yourfilename.png
    
    <div id="yourfilename" class="sprite-png yourfilename"></div>

All the sprites, HTML, CSS and JS files will output to the corresponding *dist/...* folder.

### HTML

HTML files in *src* are vanilla HTML except for the added ability to embed external text based files (HTML, CSS, JS, SVG). You can do this by inserting the following special comment where you want:

    <!-- include project-path-to-asset/file.svg -->
    <!-- include project-path-to-asset/file.min.js -->

**NOTE:** The path that will look for the files is absolute to the project root folder and not where the main.html is.  
**NOTE 2:** JS and CSS includes are automatically wrapped in opening and closing &lt;script&gt; and &lt;style&gt; tags respectively.

![html example](docs/images/code-html.png)

### ES6 Modules

Every main.js file in any *src/...* folder functions as an entry point to import ES6 modules using Rollup as a bundler, ESLint for linting and Babel to transpile down to an ES5 *bundle.js* file inside each corresponding *dist/...* folder.
Errors found by ESLint will stop the bundle process.

More info:
* [Understanding ES6 Modules](https://www.sitepoint.com/understanding-es6-modules/)
* [Rollup module bundler](https://rollupjs.org/)
* [Babel transpiler](http://babeljs.io/)
* [ESLint](http://eslint.org/)

![html example](docs/images/code-js.png)

### SCSS

Every *main.scss* file will be compiled to *main.css* in the *styles* directory. From there, all CSS files in the *styles* directory (such as spritesheet CSS files) are combined and autoprefixed before being outputted to the file *bundle.css* in the corresponding **dist/...** folder.

![html example](docs/images/code-sass.png)

## Contributing

If you plan on adding features, fixing bugs or contributing to this template then you should clone this repo by doing:

    git clone git@gitlab.ny.rga.com:digitaladvertising/templates/npm-banner-cd.git

Please read [CONTRIBUTING.md](docs/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests.

### Useful links

Guides to using NPM as a build tool:

* [Why NPM scripts, by CSS Tricks](https://css-tricks.com/why-npm-scripts/)
* [How to use NPM as a build tool, by Keith Cirkel](https://www.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/)
* [Introduction to using NPM as a build tool, @ Medium](https://medium.com/javascript-training/introduction-to-using-npm-as-a-build-tool-b41076f488b0)

## Versioning

We use [SemVer](http://semver.org/) for versioning. More info can be found in [CHANGELOG.md](docs/CHANGELOG.md).

## License

This project is licensed under the ISC License - see the [LICENSE.md](docs/LICENSE.md) file for details.

■ R/GA
