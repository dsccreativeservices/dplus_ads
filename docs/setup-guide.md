# Setup Environment Guide

This guide will cover step by step how to install the needed software for working with the project.

## Installation on macOS & GNU/Linux

### Node.js + NPM

Start by checking if you already have Node.js and NPM installed on your machine.
Do this by opening your terminal or command line and typing the following commands:

    node -v
    npm -v

If you don't have them installed, then the best way to install and manage Node.js is by using [NVM (Node Version Manager)](https://github.com/creationix/nvm) instead of using the official Node.js installer.
This allows you to have multiple Node.js versions and do easy upgrades/downgrades as necessary.

You can install it by using the script from [this instructions](https://github.com/creationix/nvm#install-script).
Once installed restart your terminal and do:

    nvm install node

You should check the versions installed to be sure that everything is runnning as expected.

### Git

As with Node, start by checking if you have Git by doing:

    git --version

If you are on macOS and you don't have it installed, it should prompt you to install it.
You can follow that wizard and get it up and running.

If you don't get that prompt, you can install the Xcode Command Line Tools by doing:

    xcode-select -install

Just follow the steps and you'll be ready.

## Installation on Windows

### Set up WSL

In order to set up a safe working environment you should use Windows Subsystem for Linux. This allows you to have bash, SSH, and other Unix commands available globally.

First start by installing a Linux distro from WSL following the official documentation from [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
This guides uses Ubuntu as a base system since it has a lot of packages bundled out of the box.

**NOTE:** You can use other Linux distros but the instructions could vary case by case and it may not work.

Once you have your Linux VM up and running, do:

    sudo apt update && sudo apt upgrade

After the system is up to date, you can proceed to do the Node.js installation.

### Node.js + NPM

You can install it by using the script from [this instructions](https://github.com/creationix/nvm#install-script).
Once installed restart your terminal and do:

    nvm install node

You should check the versions installed to be sure that everything is runnning as expected.

**NOTE:** If you receive an error saying that nvm wasn't found, please be sure to restart your terminal and try again.

With that, you have a fully working Linux VM set up for all your banner needs. If something goes wrong you can always uninstall the Linux VM and start from scratch.

### WSL: Giving Linux VM write access to your Windows drive

In order to have write access to your mounted Windows drives you have to configure WSL to be able to do it automatically.
You'll have to create a WSL config file that will manage all of this. With your Linux terminal open do:

    sudo touch /etc/wsl.conf && sudo nano /etc/wsl.conf

This will create the file and open to edit it. Add the following lines to it:

    [automount]
    options = "metadata"

Save it and close it by doing CTRL+O to save and then CTRL+X to exit.

Now you have to restart the Linux VM to take those settings into effect.
From a PowerShell terminal and do:

    wsl --shutdown

This will make all your Linux VM to turn off.
Simply relaunch the Linux VM again to make it available again.

### WSL: Navigating to your project folder easily in Windows

Since you have WSL running on Windows, you have full access to the hard drive. Ideally, you should always manage your files from Windows and not the other way around.

Simply open Ubuntu and navigate to your project folder.

Alternatively you can open PowerShell, cmd, or the Windows Terminal and do:

    bash

This will let you execute bash commands natively and execute NPM scripts. Just navigate using 'cd' to you project folder and have fun.

All your hard-drives are accessible through the /mnt/ folder. For example you can go to your Windows user folder by doing

    cd mnt/c/Users/<your username>

Alternatively, you can open the contextual menu while holding shift on the project folder and select to open a new Linux shell from there.

**NOTE:** If Windows Firewall prompts you to allow or deny the node process, please allow it. Denying could brake the funciontality of the banner.