# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.7] - 2020-07-30
## Added
- Global folder inside lib/include.
- Added usePolling to chokidar watcher for better file compatibility in WSL enviroments.

## Changed
- Updated docs/setup-guide.md with an up to date guide on how to use the banner in Windows 10 with WSL.
- GSAP updated to v3.4.2.
- devDependencies updated.
- Minor stuff in package.json.
- Moved HTML global includes into its own folder.
- Moved Mustache template for SVG Sprite into the modules folder.
- Reordered the src/main.js file a little bit to make it simpler.

## Fixed
- Minor bugs in review index main.js file.

## Removed
- Legacy GSAP v2. There was no use at all.
- Legacy GSAP v2 globals from package.json.
- devDependency imagemin-gifsicle since it wasn't used and made problems with WSL enviroments.

## [2.0.6] - 2020-07-16
## Fixed
- Fixed a bug that caused the replace HTML head tags function to run on each HTML include and failing to run properly.

## [2.0.5] - 2020-07-03
## Added
- HTML ad.size and title are populated automatically from its folder name.
- _head.html in /lib/include containing the entire HTML head tag.
- _scripts.html in /lib/include containing JS scripts.
- img selector is position absolute now.

## Changed
- GSAP updated to v3.3.4.
- devDependencies updated.
- Cleaned up main.js file.
- Renamed some functions with more semantic names.
- SCSS output style changed to compact.

## Removed
- Removed log import module from main.js since it wasn't used.

## [2.0.4] - 2020-05-07
## Fixed
- R/GA Showcase title wasn't outputting the project title.

## [2.0.3] - 2020-04-30
## Added
- Centering SCSS file imported by default in main.scss.
- Ad backgound is now controlled by a variable.
- CSS class to fix black lines around JPGs due to scaling issues.

## [2.0.2] - 2020-04-28
## Added
- /dist/300x600 folder to avoid CI/CD issues.
- clicktag.html in /lib/include.

## Changed
- Separated Node tasks modules from ES6 modules.
- ES6 modules stay on /lib/modules.
- Node tasks modules now live in /lib/tasks/modules.
- Renamed src/blank-300x600 folder to just src/300x600.
- Moved base.scss from /lib/sass/abstracts to /lib/sass.
- Moved /lib/templates to /lib/tasks/templates.

## Removed
- Stackblur files from /lib/include and /lib/modules/vendor.

## [2.0.1] - 2020-04-27
### Changed
- Moved README.md visual examples into their sections.

### Fixed
- Typos in README.md

### Removed
- Version number from README.md

## [2.0.1] - 2020-04-20
### Changed
- GSAP updated to v3.2.6.
- devDependencies updated.

## [2.0.0] - 2020-03-09
### Added
- Docs folder.
- CHANGELOG.md, CONTRIBUTING.md, and LICENSE.md files in /docs.
- Setup guide in /docs.
- Added some new globals to package.json.
- Offline minified versions of TweenMax and Plugins for v2.1.3 and v3.2.4.

### Changed
- Updated all devDependecies to latest versions.
- Assets folder now lives inside /lib.
- Moved transfonter-settings.txt to /docs.
- GSAP JS files now live in /lib/gsap for v2 and v3 separately.
- All template JS modules now live in /lib/modules/internal.
- All 3rd party JS modules now live in /lib/modules/vendors.
- Creative specific JS modules stay on /lib/modules.
- Generic SCSS styles now live in /lib/sass/abstracts.
- Fonts SCSS styles now live in /lib/sass/fonts.
- Creative partials stay on /lib/sass.
- Updated all JS modules and tasks to point these new directories.
- GSAP updated to v3.2.4.
- main.js is GSAP3 ready with an empty timeline.
- Restructured main.scss with new @imports and variables.
- Review index page is mobile-first responsive now.
- Reworked README.md file.

### Removed
- Cleaned up package.json a little bit.
- Uneeded stuff from /lib/include.
- Sizmek template since it wasn't used anymore.
- Examples folder since it uses old GSAP version, to avoid confusions. Ideally there should be a new GSAP3 examples folder.